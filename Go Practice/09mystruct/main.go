package main

import "fmt"

func main() {
	fmt.Println("Structs in golang")

	// no inheritance in golang, no super or parent

	tareque := User{"sam", "tarequensu@gmail.com", true, 34}

	fmt.Println(tareque)
	fmt.Printf("tareque's details are %+v\n", tareque)
	fmt.Printf("Name is %v and email is %v\n", tareque.Name, tareque.Email)
}

type User struct {
	Name   string
	Email  string
	Status bool
	Age    int
}
