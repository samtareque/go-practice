package main

import "fmt"

func main() {
	fmt.Println("Methods in golang")

	// no inheritance in golang, no super or parent

	tareque := User{"sam", "tarequensu@gmail.com", true, 34}

	fmt.Println("Information assigned to struct", tareque)
	fmt.Printf("tareque's details infomation using Printf are %+v\n", tareque)
	fmt.Printf("Name is %v and email is %v\n", tareque.Name, tareque.Email)

	tareque.GetStatus()
	tareque.NewEmail()

	fmt.Printf("Name is %v and email is %v\n", tareque.Name, tareque.Email)

}

type User struct { //declaring a struct
	Name   string
	Email  string
	Status bool
	Age    int
}

func (u User) GetStatus() { // constructing a method GetStatus(), object u and type is User
	fmt.Println("User's name :", u.Name)
	fmt.Println("Is user active :", u.Status)

}

func (u User) NewEmail() { // constructing a method NewEmail()

	u.Email = "tareqsam@b-tu.de"
	fmt.Println("Email of this user is:", u.Email)
}
