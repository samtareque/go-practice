package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {

	fmt.Println("Welcome to files in golang")
	content := "This needs to go in a file - LearnCodeOnline.in"

	file, err := os.Create("./mylcogofile.text") // comma , ok syntext
	checkNilErr(err)

	// if err != nil {

	// 	panic(err)
	// }

	length, err := io.WriteString(file, content)
	checkNilErr(err)

	// if err != nil {

	// 	panic(err)
	// }

	fmt.Println("Length is :", length)

	defer file.Close()

	readFile("./mylcogofile.text")

}
func readFile(filename string) {

	databyte, err := ioutil.ReadFile(filename)

	checkNilErr(err)

	// if err != nil {

	// 	panic(err)
	// }

	fmt.Println("Text data inside the file is\n", string(databyte))
}
func checkNilErr(err error) {

	if err != nil {

		panic(err)
	}

}
