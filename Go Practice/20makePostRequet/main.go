package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func main() {
	fmt.Println("Welcome to PerfomPostJSONrequest")

	// PerformGetRequest()
	PerformPostJsonRequest()
}

func PerformGetRequest() {

	const myurl = "http://localhost:1111/get"

	response, err := http.Get(myurl) // httpd is library, get is method

	if err != nil {

		panic(err)

	}

	defer response.Body.Close()

	content, _ := ioutil.ReadAll(response.Body)

	fmt.Println("Status Code:", response.Status)
	fmt.Println("Content length is:", response.ContentLength)

	fmt.Println("Content length/byte count ", content)                  // in bytes
	fmt.Println("What is the Content in normal way: ", string(content)) // actual content

	var responseBuilder strings.Builder

	byteCount, _ := responseBuilder.Write(content) // write is method

	fmt.Println("Content length/byte count  using strings package: ", byteCount)
	fmt.Println("Actual content using responseBuilder strings package :", responseBuilder.String())

}

func PerformPostJsonRequest() {

	const myurl = "http://localhost:1111/post"

	//fake json payload

	requestBody := strings.NewReader(`
	{
	"coursename":"Let's go with golang",
	"price": 0,
	"platfrom": "learncode"
	}
	
	`)

	response, err := http.Post(myurl, "application/json", requestBody)

	if err != nil {

		panic(err)
	}
	defer response.Body.Close()

	fmt.Println("Status Code:", response.Status)
	fmt.Println("Content length is:", response.ContentLength)

	content, _ := ioutil.ReadAll(response.Body)

	fmt.Println("Content length/byte count", content)
	fmt.Println("What is the content:", string(content))

}
