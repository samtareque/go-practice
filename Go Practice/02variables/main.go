package main

import "fmt"

const LoginToken string = "mogambo" // with capital we declare public variable

func main() {

	var username string = "tareque"
	fmt.Println(username)
	fmt.Printf("Variable is of type : %T \n", username)

	var smallVal uint8 = 255
	fmt.Println(smallVal)
	fmt.Printf("Variable is of type : %T \n", smallVal)

	var smallFloat float64 = 255.3343535353535
	fmt.Println(smallFloat)
	fmt.Printf("Variable is of type : %T \n", smallFloat)

	// default values and some aliases

	var anothervarialbe int
	fmt.Println(anothervarialbe)
	fmt.Printf("Variable is of type : %T \n", anothervarialbe)

	// implicit type

	var website = "tareque.com"
	fmt.Println(website)

	// no var style

	numberOfusers := 786 // volarious declartion
	fmt.Println(numberOfusers)

	fmt.Println(LoginToken)
	fmt.Printf("Variable is of type : %T \n", LoginToken)

}
