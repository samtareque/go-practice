package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

const url = "https://lco.dev"

func main() {
	fmt.Println("LCO web request")

	response, err := http.Get(url)

	if err != nil {

		panic(err)
	}

	fmt.Printf("Type of the response %T\n", response)

	defer response.Body.Close() // caller's responsibility to close the connection

	responseInBytes, err := ioutil.ReadAll(response.Body)

	if err != nil {

		panic(err)
	}

	fmt.Println("Response in Bytes:", responseInBytes)

	realContent := string(responseInBytes)

	fmt.Println("Real Content of the response", realContent)

}
