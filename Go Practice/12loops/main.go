package main

import "fmt"

func main() {
	fmt.Println("Let's learn loop in golang")
	// let's define a slice of days
	days := []string{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}

	// fmt.Println("Weekday :", days)

	// for d := 0; d < len(days); d++ {
	// 	fmt.Println("Weekday: ", d, days[d])
	// }

	// for i := range days {

	// 	fmt.Println("Weekday: ", i, days[i])

	for index, day := range days {

		fmt.Printf("Index is %v and day is %v\n", index, day)
	}

	rogueValue := 1

	// for rogueValue < 10 {

	// 	if rogueValue == 5 {

	// 		break // will get out from the for loop

	// 	}

	for rogueValue < 10 {

		if rogueValue == 4 {

			goto lco

		}

		if rogueValue == 5 {
			rogueValue++
			continue // will skip the current value and get back to check the condition here rougueValue < 10

		}

		fmt.Println("value is :", rogueValue)
		rogueValue++

	}

lco: // goto statement

	fmt.Println("Jumping at LearnCodeOnline.in")

}
