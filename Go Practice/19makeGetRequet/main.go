package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func main() {
	fmt.Println("Welcome to web verb video")

	PerformGetRequest()
}

func PerformGetRequest() {

	const myurl = "http://localhost:1111/get"

	response, err := http.Get(myurl) // httpd is library, get is method

	if err != nil {

		panic(err)

	}

	defer response.Body.Close()

	fmt.Println("Status Code:", response.Status)
	fmt.Println("Content length is:", response.ContentLength)

	var responseBuilder strings.Builder

	content, _ := ioutil.ReadAll(response.Body)

	fmt.Println("Content in byte", content)              // in bytes
	fmt.Println("Content normal way: ", string(content)) // actual content

	byteCount, _ := responseBuilder.Write(content) // write is method

	fmt.Println("Content length/byte count  using strings package: ", byteCount)
	fmt.Println("Actual content using responseBuilder strings package :", responseBuilder.String())

}
