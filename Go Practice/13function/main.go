package main

import "fmt"

func main() {

	fmt.Println("Welcome to function in golang")

	greeter()

	result := adder(4, 5)

	proResult, myMessage := proAdder(2, 7, 9)

	fmt.Println("result is:", result)
	fmt.Println("proResult is:", proResult)
	fmt.Println("message from function return is:", myMessage)

}

func adder(vala int, valb int) int { // normal function, argument and return type int

	return vala + valb
}

// func proAdder(values ...int) int { //variadic function, argument type and return type is int

// 	total := 0

// 	for _, val := range values {

// 		total += val

// 	}

// 	return total

// }

func proAdder(values ...int) (int, string) { //variadic function, argument type int
	// and return type  are int and string

	total := 0

	for _, val := range values {

		total += val

	}

	return total, "Returning string"

}

func greeter() {
	fmt.Println("Assalamualikum")
}
