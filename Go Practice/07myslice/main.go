package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println("Welcome to learing slice in GO")

	var fruitList = []string{"Apple", "Banana", "Mango"}
	fmt.Printf("Type of frulitList is %T\n", fruitList)

	fruitList = append(fruitList, "Orange", "Guava")
	fmt.Println(fruitList)

	fruitList = append(fruitList[1:3])

	fmt.Println(fruitList)

	highScore := make([]int, 4)

	highScore[0] = 234
	highScore[1] = 934
	highScore[2] = 434
	highScore[3] = 834
	//highScore[4] = 534

	highScore = append(highScore, 555, 666, 234)

	fmt.Println(highScore)

	sort.Ints(highScore)

	fmt.Println(highScore)

	fmt.Println(sort.IntsAreSorted(highScore))

	// to remove a value from slices based on Index

	var courses = []string{"reactjs", "javascript", "swift", "python", "ruby"}

	fmt.Println(courses)

	var index int = 2

	courses = append(courses[:index], courses[index+1:]...)

	fmt.Println(courses)

}
