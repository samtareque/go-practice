package main

import "fmt"

func main() {
	fmt.Println("Welcome to class on pointers")

	// var ptr *int

	// fmt.Println("Value of pointer is", ptr)

	myNumber := 23

	var ptr = &myNumber //& ampersend means the reference

	fmt.Println("The address of the myNumber: ", &myNumber)

	fmt.Println("The address value which is stored by the pointer: ", ptr)
	fmt.Println("The value inside  of the pointer:", *ptr) // * asterisk means

	*ptr = *ptr + 5

	fmt.Println("Now the value inside of the pointer:", *ptr)
	fmt.Println("Now the value of myNumber", myNumber)

}
