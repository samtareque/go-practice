package main

import "fmt"

func main() {

	defer fmt.Println("Hello") // will be printerd after at the end like LIFO
	defer fmt.Println("my")
	fmt.Println("World") // World my hello
	myDefer()
}

func myDefer() {
	count := 10
	for i := 0; i < count; i++ {

		fmt.Println("Number:", i)

		// defer fmt.Println("Number:", i) // reverse order will be printed

	}

}
